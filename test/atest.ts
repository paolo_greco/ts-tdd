import {Model} from '../src/model';
import {expect} from 'chai';
import {} from 'mocha';


describe('model', () => {
    it('should return nothing when unconfigured', () => {
        let m = new Model();
        expect(m.do()).to.eql(null);
    })
});